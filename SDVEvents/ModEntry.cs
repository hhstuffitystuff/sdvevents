﻿using StardewModdingAPI;
using StardewModdingAPI.Events;
using StardewValley;
using Microsoft.Xna.Framework;
using System;
using System.IO;
using SDVUtility;
using System.Collections.Generic;
using SDVEvents.SDVEventConfiguration;

namespace SDVEvents
{
    /// <summary>The mod entry point.</summary>
    public class ModEntry : Mod
    {

        private bool startingMod = true;

        EventCoordinator thisEventCoordinator;

        /*********
        ** Public methods
        *********/
        /// <summary>Initialise the mod.</summary>
        /// <param name="helper">Provides methods for interacting with the mod directory, such as read/writing a config file or custom JSON files.</param>
        public override void Entry(IModHelper helper)
        {    
            SaveEvents.AfterLoad += this.AfterLoadEvent;
            SaveEvents.BeforeSave += this.BeforeSaveEvent;
            TimeEvents.DayOfMonthChanged += this.Event_NewDayEvents;
        }


        /*********
        ** Private methods
        *********/

        private void BeforeSaveEvent(object sender, EventArgs e)
        {
            if(thisEventCoordinator != null)
            {
                thisEventCoordinator.Save();
            }
        }
        private void AfterLoadEvent(object sender, EventArgs e)
        {
            InitializeMod();
        }

        private void AfterSaveEvent(object sender, EventArgs e)
        {
            InitializeMod();
            SaveEvents.AfterSave -= this.AfterSaveEvent;
        }

        private void InitializeMod()
        {
            if (Constants.SaveFolderName.Equals(""))
            {
                this.Monitor.Log("Game isn't read to initialize mod yet (new game).  Registering InitializeMod for an AfterSave event trigger instead.");
                SaveEvents.AfterSave += this.AfterSaveEvent;
                return;
            }
            this.Monitor.Log($"Mod initialized.", LogLevel.Info);
            

            // read file
            string configPath = $"{Constants.SaveFolderName}/config.json";
            
            this.Monitor.Log("Loading Config File:" + this.Helper.DirectoryPath + "/" + configPath);
            var model = this.Helper.ReadJsonFile<SDVEventsModData>(configPath);

            if (model == null)
            {
                model = new SDVEventsModData();
                model.configPath = configPath;
                this.Helper.WriteJsonFile<SDVEventsModData>(configPath, model);
            }

            model.configPath = configPath;

            this.thisEventCoordinator = EventCoordinator.Instance(model,this.Monitor,this.Helper);
        }

        private void Event_NewDayEvents(object sender, EventArgsIntChanged e)
        {
            if (startingMod)
            {
                startingMod = false;
                this.Monitor.Log($"Starting mod. Don't check for new event on first day after a load (it should have happened just before the save occured before shutdown.");
            } else
            {
                Dictionary<int, string> dictionary = Game1.content.Load<Dictionary<int, string>>("Data\\Crops");

                SDVDate yesterday = SDVDate.Now().Yesterday();

                if (this.thisEventCoordinator != null)
                {
                    //Check for a new event. If one triggers, register it.
                    this.Monitor.Log($"Trigger event with inputs of {yesterday.Day} {yesterday.Season}, Year {yesterday.Year}.");
                    if (this.thisEventCoordinator.checkForEvent())
                    {
                        this.Monitor.Log($"Event should trigger.");
                        LifeEvent eventToTrigger = this.thisEventCoordinator.getEvent(yesterday.Season, yesterday.Day);
                        this.thisEventCoordinator.registerEvent(eventToTrigger);
                    }
                    else
                    {
                        this.Monitor.Log($"Event does not trigger.");
                    }

                    //Now handle basic event maintenance.
                    this.thisEventCoordinator.runEventMaintenance();
                }
                else
                {
                    this.Monitor.Log($"No Event Coordinator instantiated yet.");
                }
            }
        }
    }
}