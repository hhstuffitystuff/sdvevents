﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SDVEvents.SDVEventConfiguration;

namespace SDVEvents.SDVEventConfiguration
{
    class EventConfigurationJSONConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(EventConfiguration).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jObject = JObject.Load(reader);

            var targetObject = new EventConfiguration();
            
            foreach(JToken token in jObject.Children())
            {
                token.ToString();
            }

            return targetObject;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            EventConfiguration thisConfig = (EventConfiguration)value;

            JObject objectToLoad = JObject.FromObject(thisConfig.dictionary);

            writer.Formatting = Formatting.Indented;
            objectToLoad.WriteTo(writer);
        }
        
    }
}
