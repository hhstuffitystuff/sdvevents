﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDVEvents.SDVEventConfiguration
{
    class SDVEventsModData
    {
        /// <summary>
        /// Maximum number of events that can occure per season.
        /// Defaults to 4 events per season.
        /// A maximum of one event will start each day.
        /// Events can run for a length of time however, so it is possible for events to overlap.
        /// </summary>
        public int maxEventsPerSeason { get; set; } = 4;
        public double eventChancePerDayAsPercentage { get; set; } = 10.00;

        public string configPath { get; set; } = "";

        private Dictionary<LifeEventType, bool> _eventTypesActives;

        public Dictionary<LifeEventType,bool> eventTypesStatus
        {
            get
            {
                return _eventTypesActives;
            }
            set
            {
                setupEventTypesDictionary(value);
            }
        }

        public SDVEventsModData()
        {
            setupEventTypesDictionary();
        }

        private void setupEventTypesDictionary(Dictionary<LifeEventType, bool> incomingDictionary = null)
        {
            if (eventTypesStatus == null || incomingDictionary == null)
            {
                _eventTypesActives = new Dictionary<LifeEventType, bool>();
                foreach (LifeEventType eventType in Enum.GetValues(typeof(LifeEventType)))
                {
                    _eventTypesActives.Add(eventType, true);
                }
            }
            else
            {
                foreach (LifeEventType eventType in Enum.GetValues(typeof(LifeEventType)))
                {
                    if (_eventTypesActives.ContainsKey(eventType) && incomingDictionary.ContainsKey(eventType))
                    {
                        _eventTypesActives[eventType] = incomingDictionary[eventType];
                    }
                    else
                    {
                        _eventTypesActives.Add(eventType, true);
                    }

                }
            }
        }
    }
}
