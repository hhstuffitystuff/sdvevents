﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDVEvents.SDVEventConfiguration
{
    public class EventConfiguration : DynamicObject
    {
        public EventDictionary dictionary = new EventDictionary();
        

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            //underlying dictionary will always return, even if it's null.  This allows null checks.
            result = dictionary[binder.Name];
            return true;
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            dictionary[binder.Name] = value;
            return true;
        }

        static public EventConfiguration ReadDefaultConfiguration(string defaultDirectory, string overrideDirectory, string fileName)
        {
            FileInfo configFile;
            FileInfo overrideConfigeFile = new FileInfo(overrideDirectory + fileName + ".json");
            FileInfo defaultConfigFile = new FileInfo(defaultDirectory + fileName + ".json");
            if (overrideConfigeFile.Exists)
            {
                configFile = overrideConfigeFile;
            }
            else if (defaultConfigFile.Exists)
            {
                configFile = defaultConfigFile;
            }
            else
            {
                throw new FileNotFoundException("No config file found for " + fileName);
            }

            try
            {
                SDVEventConfiguration.EventConfiguration configuration = JsonConvert.DeserializeObject<EventConfiguration>(File.ReadAllText(configFile.FullName));
                return configuration;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        static public void SaveDefaultConfiguration(string defaultDirectory, string overrideDirectory, string fileName, EventConfiguration configToSave)
        {
            FileInfo configFile;
            FileInfo overrideConfigeFile = new FileInfo(overrideDirectory + fileName + ".json");
            FileInfo defaultConfigFile = new FileInfo(defaultDirectory + fileName + ".json");

            if (!defaultConfigFile.Exists)
            {
                configFile = defaultConfigFile;
            }
            else if (!overrideConfigeFile.Exists)
            {
                configFile = overrideConfigeFile;
            }
            else
            {
                throw new ArgumentException("Default Config Files exist both globally and for this save. Cannot save.");
            }

            try
            {
                FileStream config;
                if (!configFile.Exists)
                {
                    if (!configFile.Directory.Exists)
                    {
                        configFile.Directory.Create();
                    }

                    config = configFile.Create();
                } else
                {
                    config = new FileStream(configFile.FullName,FileMode.Append);
                }    

                using (StreamWriter sWriter = new StreamWriter(config))
                {
                    string serialized = JsonConvert.SerializeObject(configToSave,new EventConfigurationJSONConverter());
                    sWriter.Write(serialized);
                }   
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }

    public class EventDictionary : IDictionary<string, object>
    {
        protected Dictionary<string, object> _internal = new Dictionary<string, object>();

        public object this[string key]
        {
            get
            {
                //return even if the value isn't present.
                object val = null;
                _internal.TryGetValue(key, out val);
                return val;
            }
            set
            {
                _internal[key] = value;
            }
        }

        public int Count
        {
            get
            {
                return _internal.Count();
            }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public ICollection<string> Keys
        {
            get
            {
                return _internal.Keys;
            }
        }

        public ICollection<object> Values
        {
            get
            {
                return _internal.Values;
            }
        }

        public void Add(KeyValuePair<string, object> item)
        {
            this.Add(item.Key,item.Value);
        }

        public void Add(string key, object value)
        {
            _internal.Add(key, value);
        }

        public void Clear()
        {
            _internal.Clear();
        }

        public bool Contains(KeyValuePair<string, object> item)
        {
            return _internal.Contains(item);
        }

        public bool ContainsKey(string key)
        {
            return _internal.ContainsKey(key);
        }

        public void CopyTo(KeyValuePair<string, object>[] array, int arrayIndex)
        {
            ((IDictionary<string, object>)_internal).CopyTo(array, arrayIndex);
        }

        public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
        {
            return ((IDictionary<string, object>)_internal).GetEnumerator();
        }

        public bool Remove(KeyValuePair<string, object> item)
        {
            return ((IDictionary<string, object>)_internal).Remove(item);
        }

        public bool Remove(string key)
        {
            return _internal.Remove(key);
        }

        public bool TryGetValue(string key, out object value)
        {
            return _internal.TryGetValue(key, out value);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _internal.GetEnumerator();
        }
    }
}
