﻿using SDVUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDVEvents
{
    /// <summary>
    /// An event that simply robs the player of 5% to 15% of their gold.  The maximum amount of gold this event will steal is a range between 500-1500 gold.
    /// </summary>
    class RobberyEvent : LifeEvent
    {
        public RobberyEvent()
        {   
            ApplicableSeasons = SeasonHelper.getAllSeasons();
            EventTypes.Add(LifeEventType.ATTACK);
        }

        public override void triggerMainEventAction()
        {
            if (shouldTrigger())
            {
                StardewValley.Farmer playerCharacter = StardewValley.Game1.player;
                double fivePercentOfGold = Math.Min(Math.Floor(playerCharacter.Money * .05), 500.0);
                double fifteenPercentOfGold = Math.Min(Math.Ceiling(playerCharacter.Money * 0.15), 1500.0);
                int amountToSteal = rng.Next((int)fivePercentOfGold, (int)fifteenPercentOfGold);

                playerCharacter.money -= amountToSteal;

                SDVUtility.Messaging.ShowMessage($"You have been robbed of {amountToSteal} gold in the night!", SDVUtility.MessageType.Important);
                lastTriggerDate = SDVDate.Now();
            }
        }

        protected override string getDefaultDescription()
        {
            return "A theif in the night has stolen some of your gold!";
        }

        protected override string getDefaultName()
        {
            return "Robbery";
        }
    }
}
