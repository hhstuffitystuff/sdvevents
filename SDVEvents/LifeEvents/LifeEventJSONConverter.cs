﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace SDVEvents
{
    class LifeEventJSONConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(LifeEvent).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var jObject = JObject.Load(reader);
            var sourceType = jObject["ClassType"].Value<string>();

            try
            {
                Type specificType = Type.GetType(sourceType, true);
                if (CanConvert(specificType))
                {
                    object target = Activator.CreateInstance(specificType);

                    serializer.Populate(jObject.CreateReader(), target);

                    return target;
                } else
                {
                    throw new ArgumentException($"{sourceType} does not appear to be derived from SDVEvents.LifeEvent.");
                }
               
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
