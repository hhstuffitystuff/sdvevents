﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardewModdingAPI.Events;
using SDVUtility;
using StardewValley.Objects;
using SDVObject = StardewValley.Object;
using StardewValley;
using StardewValley.Buildings;
using StardewValley.Locations;

namespace SDVEvents
{
    /// <summary>
    /// A crop-loss event that symbolizes a rodent infestation.
    /// </summary>
    /// <remarks>
    /// The severity of this event is reduced if the player has a cat on the farm.
    /// </remarks>
    class RatInfestation : LifeEvent
    {
        //CONSTANTS. ONLY USED IF CONFIG FILES ARE MISSING.
        private const int DEFAULT_DURATION_WITHOUT_CAT = 2;
        private const int DEFAULT_DURATION_WITH_CAT = 1;
        private const double DEFAULT_PERCENT_DESTROYED_WITHOUT_CAT = 0.05;
        private const double DEFAULT_PERCENT_DESTROYED_WITH_CAT = 0.03;

        //VALUES TO USE. READ IN FROM CONFIG FILE OR IF CONFIG FILE MISSING USES ABOVE CONSTS.
        private int _durationWithCat;
        private int _durationWithoutCat;

        private double _percentDestroyedWithCat;
        private double _percentDestroyedWithoutCat;


        //The current duration and percent to destroy in the game.  Can change as conditions change.
        private int _durationInWeeks = DEFAULT_DURATION_WITHOUT_CAT;
        private double _percentOfCropsDestroyed = DEFAULT_PERCENT_DESTROYED_WITHOUT_CAT;

        public SDVDate DateStarted { get; set; } = null;

        public static bool isRunning
        {
            get;
            private set;
        }
        public RatInfestation()
        {
            resetEventValues();
        }

        public override bool RegisterEvent(EventCoordinator coordinator)
        {
            //register only a single ratinfestation event at a time.
            if (!isRunning)
            {
                if (DateStarted == null)
                {
                    DateStarted = SDVDate.Now();
                }
                TimeEvents.DayOfMonthChanged += this.Event_NewDayEvent;
                isRunning = true;
                this.registerEventCoordinator(coordinator);
                triggerMainEventAction();
                return true;
            } else
            {
                return false;
            }
            
        }

        public override bool DeregisterEventIfAppropriate()
        {
            SDVDate today = SDVDate.Now();
            SDVDate lastDayOfEvent = DateStarted.Add(_durationInWeeks, SDVDateInterval.WEEK);
            if(today >= lastDayOfEvent)
            {
                TimeEvents.DayOfMonthChanged -= this.Event_NewDayEvent;
                isRunning = false;
                string messageToDisplay = "";
                if (Game1.player.hasCat())
                {
                    messageToDisplay = String.Format("{0} has driven the rats away from your farm.", Game1.player.getPetName());
                } else
                {
                    messageToDisplay = String.Format("You have finally managed to drive the rats away.");
                }
                Messaging.ShowMessage(messageToDisplay, MessageType.Important);
                return true;
            }
            return false;
        }

        public override void triggerMainEventAction()
        {
            if (shouldTrigger())
            {
                resetEventValues();
                List<Chest> playerChests = new List<Chest>();
                foreach (StardewValley.GameLocation currentLocation in StardewValley.Game1.locations.Concat(Game1.getFarm().buildings.Select(p => p.indoors).Where(p => p != null)))
                {
                    foreach (SDVObject item in currentLocation.objects.Values)
                    {
                        if (item is Chest)
                        {
                            Chest chest = item as Chest;
                            if (chest.playerChest)
                            {
                                playerChests.Add(chest);
                            }
                        }
                    }

                    // destroy items stored in farm buildings
                    if (currentLocation is Farm)
                    {
                        foreach (var building in (currentLocation as Farm).buildings)
                        {
                            if (building is Mill)
                                playerChests.Add((building as Mill).output);
                            //items.AddRange((building as Mill).output.items);
                            else if (building is JunimoHut)
                                playerChests.Add((building as JunimoHut).output);
                            //items.AddRange((building as JunimoHut).output.items);
                        }
                    }
                }

                int totalItemsDestroyed = 0;

                foreach (Chest playerChest in playerChests)
                {
                    totalItemsDestroyed += deletePercentOfItems(playerChest, _percentOfCropsDestroyed);
                }
                if (totalItemsDestroyed > 0)
                {
                    SDVUtility.Messaging.ShowMessage(String.Format("Rats have ruined some of your stored goods. {0} items lost.", totalItemsDestroyed), MessageType.ItemLost);
                }
                lastTriggerDate = SDVDate.Now();
            }
            
            
        }

        private int deletePercentOfItems(Chest container, double percentage)
        {
            int totalItemsDestroyed = 0;
            List<Item> itemsToRemove = new List<Item>();
            foreach (Item item in container.items)
            {
                int numberToDestroy = 0;
                var cat = SDVUtility.ObjectCategory.findByValue(item.category);
                if (ObjectCategory.isFood(cat) || cat.Equals(ObjectCategory.SEEDS))
                {
                    int threshHold = (int)Math.Ceiling((100.0 / (percentage * 100.0)));
                    bool stackUnderThreshhold = item.Stack < threshHold;
                    this.parentCoordinator.Log(this, $"{item.Name}: stack size {item.Stack} < {threshHold} is {stackUnderThreshhold}");
                    if (stackUnderThreshhold)
                    {
                        for(int itemCounter = 1; itemCounter <= item.Stack; itemCounter++)
                        {
                            double triggerValue = rng.NextDouble();
                            bool destroyItem = triggerValue <= percentage;
                            this.parentCoordinator.Log(this, $"{item.Name}: trigger value of {triggerValue} <= {percentage} is {destroyItem}");
                            if (destroyItem)
                            {
                                numberToDestroy += 1;
                            }
                        }
                    } else
                    {   
                        numberToDestroy = (int)(Math.Ceiling(item.Stack * percentage));
                    }

                    item.Stack -= numberToDestroy;
                    this.parentCoordinator.Log(this, $"{item.Name}: Destroying {numberToDestroy}.");
                    if (item.Stack == 0)
                    {
                        itemsToRemove.Add(item);
                        this.parentCoordinator.Log(this, $"{item.Name}: Last item in stack destroyed. Removing from container.");
                    }

                    totalItemsDestroyed += numberToDestroy;
                }
            }
            foreach(Item toRemove in itemsToRemove)
            {
                container.items.Remove(toRemove);
            }
            return totalItemsDestroyed;
        }

        private void Event_NewDayEvent(object caller, EventArgsIntChanged e)
        {
            this.triggerMainEventAction();
        }

        private void resetEventValues()
        {
            if (Game1.player.hasCat())
            {
                _durationInWeeks = _durationWithCat;
                _percentOfCropsDestroyed = _percentDestroyedWithCat;
            } else
            {
                _durationInWeeks = _durationWithoutCat;
                _percentOfCropsDestroyed = _percentDestroyedWithoutCat;
            }
        }

        protected override bool BuildFromConfiguration()
        {
            bool hasConfiguration = base.BuildFromConfiguration();

            try
            {
                this._durationWithCat = (int)Configuration.DurationWithCat;
            }
            catch (Exception e)
            {
                if (hasConfiguration)
                {
                    parentCoordinator.Log(this, "Error while loading configuration: " + e.Message);
                }
                
                this._durationWithCat = DEFAULT_DURATION_WITH_CAT;
                Configuration.DurationWithCat = this._durationWithCat;
            }

            try
            {
                this._durationWithoutCat = (int)Configuration.DurationWithoutCat;
            }
            catch (Exception e)
            {
                if (hasConfiguration)
                {
                    parentCoordinator.Log(this, "Error while loading configuration: " + e.Message);
                }

                this._durationWithoutCat = DEFAULT_DURATION_WITHOUT_CAT;
                Configuration.DurationWithoutCat = this._durationWithoutCat;
            }

            try
            {
                this._percentDestroyedWithCat = (double)Configuration.PercentDestroyedWithCat;
            }
            catch (Exception e)
            {
                if (hasConfiguration)
                {
                    parentCoordinator.Log(this, "Error while loading configuration: " + e.Message);
                }

                this._percentDestroyedWithCat = DEFAULT_PERCENT_DESTROYED_WITH_CAT;
                Configuration.PercentDestroyedWithCat = this._percentDestroyedWithCat;
            }

            try
            {
                this._percentDestroyedWithoutCat = (double)Configuration.PercentDestroyedWithoutCat;
            }
            catch (Exception e)
            {
                if (hasConfiguration)
                {
                    parentCoordinator.Log(this, "Error while loading configuration: " + e.Message);
                }
                this._percentDestroyedWithoutCat = DEFAULT_PERCENT_DESTROYED_WITHOUT_CAT;
                Configuration.PercentDestroyedWithoutCat = this._percentDestroyedWithoutCat;
            }

            return hasConfiguration;
        }

        protected override String getDefaultName()
        {
            return "Rats!";
        }

        protected override String getDefaultDescription()
        {
            return "Rats have infested the farm and are eating your stored goods!";
        }

        protected override HashSet<Season> getDefaultSeasons()
        {
            return new HashSet<Season> { Season.SPRING, Season.FALL, Season.WINTER };
            
        }

        protected override HashSet<LifeEventType> getDefaultEventTypes()
        {
            this.EventTypes.Add(LifeEventType.VERMIN);
            return this.EventTypes;
        }

        protected override bool shouldTrigger()
        {
            return (this.DateStarted != null && base.shouldTrigger());
        }
    }
}
