﻿using System;
using SDVUtility;
using System.Collections.Generic;
using SDVEvents.SDVEventConfiguration;

namespace SDVEvents
{
    abstract class LifeEvent
    {
        /// <summary>
        /// The human readable name of the event.
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// A description of the event.
        /// </summary>
        public string Description { get; protected set; }

        /// <summary>
        /// A collection of the seasons that this event can show up in.
        /// </summary>
        public HashSet<Season> ApplicableSeasons { get; protected set; } = new HashSet<Season>();

        /// <summary>
        /// A collection of the LifeEventTypes that this event is part of.
        /// </summary>
        public HashSet<LifeEventType> EventTypes { get; protected set; } = new HashSet<LifeEventType>();

        /// <summary>
        /// The configuration settings for this event.
        /// </summary>
        protected dynamic Configuration;

        public string ClassType { get; private set; }

        public SDVDate lastTriggerDate;

        protected EventCoordinator parentCoordinator
        {
            get;
            private set;
        } = null;

        protected bool builtFromConfiguration = false;

        /// <summary>
        /// A random number generator for all the LifeEvent derived classes to utilize so we aren't making bunches of them.
        /// </summary>
        protected static Random rng = new Random();

        
        public LifeEvent()
        {   
            ClassType = this.GetType().FullName;
        }
        
        virtual public void triggerMainEventAction()
        {
            SDVDate today = SDVDate.Now();
            if (lastTriggerDate < today)
            {
                SDVUtility.Messaging.ShowInfoMessage("Event Triggered.");
                lastTriggerDate = today;
            }
        }

        /// <summary>
        /// Trigger the event to register itself for various events.
        /// </summary>
        /// <remarks>
        /// By default, an event will trigger instantly a single time unless RegisterEvent is overridden.
        /// </remarks>
        virtual public bool RegisterEvent(EventCoordinator coordinator)
        {
            this.registerEventCoordinator(coordinator);
            //by default, events trigger instantly a single time.
            this.triggerMainEventAction();
            return true;
        }

        virtual public bool DeregisterEventIfAppropriate()
        {
            return true;
        }

        virtual public void registerEventCoordinator(EventCoordinator coordinator)
        {   
            parentCoordinator = coordinator;
            if (!builtFromConfiguration)
            {
                if (!BuildFromConfiguration())
                {
                    SaveDefaultConfiguration(Configuration);
                }
                builtFromConfiguration = true;
            }
            
        }

        virtual protected bool shouldTrigger()
        {
            SDVDate today = SDVDate.Now();
            if (lastTriggerDate == null || lastTriggerDate < today)
            {
                return true;
            }
            return false;
        }

        protected virtual bool BuildFromConfiguration()
        {
            bool hasConfiguration = true;

            if (Configuration == null)
            {
                //configuration is null, so try to read it in from a file.
                ReadDefaultConfiguration();

                
                if(Configuration == null)
                {
                    hasConfiguration = false;
                    //configuration is STILL null, so we must not have a file to read from, so lets make a blankconfiguration and build it programatically.
                    Configuration = new EventConfiguration();
                }
            }

            #region Load Applicable Seasons.
            Newtonsoft.Json.Linq.JArray seasons = Configuration.ApplicableSeasons;
            bool setSeasonDefaults = true;
            if (seasons != null)
            {
                try
                {
                    var newHashset = new HashSet<Season>();
                    foreach (int season in seasons.Values<Int32>())
                    {
                        newHashset.Add(SeasonHelper.getSeasonByIndex(season));
                    }
                    this.ApplicableSeasons = newHashset;
                    setSeasonDefaults = false;
                } catch (Exception e)
                {
                    parentCoordinator.Log(this, "Error Loading Configuration: " + e.Message);
                }
            }
            
            if(setSeasonDefaults)
            {
                this.ApplicableSeasons = getDefaultSeasons();
                Configuration.ApplicableSeasons = this.ApplicableSeasons;
            }
            #endregion

            #region Load LifeEventTypes
            Newtonsoft.Json.Linq.JArray eventtypes = Configuration.EventTypes;
            bool setEventTypeDefaults = true;
            if (eventtypes != null)
            {
                try
                {
                    var newHashset = new HashSet<LifeEventType>();
                    foreach (int eventtype in eventtypes.Values<Int32>())
                    {
                        newHashset.Add((LifeEventType)eventtype);
                    }
                    this.EventTypes = newHashset;
                    setEventTypeDefaults = false;
                }
                catch (Exception e)
                {
                    parentCoordinator.Log(this, "Error Loading Configuration: " + e.Message);
                }
            }

            if(setEventTypeDefaults)
            {
                this.EventTypes = getDefaultEventTypes();
                Configuration.EventTypes = this.EventTypes;
            }
            #endregion


            if (Configuration.Name != null)
            {
                this.Name = Configuration.Name;
            }
            else
            {
                this.Name = getDefaultName();
                Configuration.Name = this.Name;
            }

            if (Configuration.Description != null)
            {
                this.Description = Configuration.Description;
            }
            else
            {
                this.Description = getDefaultDescription();
                Configuration.Description = this.Description;
            }

            return hasConfiguration;
        }

        protected abstract string getDefaultDescription();

        protected virtual HashSet<Season> getDefaultSeasons()
        {
            return SeasonHelper.getAllSeasons();
        }

        protected virtual HashSet<LifeEventType> getDefaultEventTypes()
        {
            EventTypes.Add(LifeEventType.GENERIC);
            return EventTypes;
        }

        protected abstract string getDefaultName();
       

        protected virtual void ReadDefaultConfiguration()
        {
            try
            {
                this.Configuration = parentCoordinator.ReadDefaultConfiguration(this.ClassType);
            } catch
            {
                parentCoordinator.Log(this, "No Configuration File Found.");
            }
            
        }

        protected virtual void SaveDefaultConfiguration(dynamic configToSave)
        {
            try
            {
                parentCoordinator.SaveDefaultConfiguration(this.ClassType,configToSave);
            }
            catch (Exception e)
            {
                parentCoordinator.Log(this, "Could Not Save Configuration: "+ e.Message);
            }
        }
    }

    public enum LifeEventType
    {
        WEATHER,VERMIN,PESTILENCE,POLLINATION,PESTKILLER,TAXATION,TAXBREAK,ATTACK,GENERIC
    }   
}
