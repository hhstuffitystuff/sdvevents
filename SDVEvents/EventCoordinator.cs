﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardewModdingAPI;
using StardewModdingAPI.Events;
using StardewValley;
using SDVUtility;
using Newtonsoft.Json;
using System.IO;
using SDVEvents.SDVEventConfiguration;

namespace SDVEvents
{
    /// <summary>
    /// A Singleton class to manage the coordination of events throughout the mod.
    /// </summary>
    class EventCoordinator
    {
        private const int minimumEventChance = 1;
        private const int maximumEventChance = 100;

        private static volatile EventCoordinator _instance;
        private static object mutexLock = new object();
        private bool registeredForSeasonalReset = false;
        
        private string saveDirectory = "";
        private string eventsPath = "";
        private string defaultEventConfigsDirectory;
        private string defaultEventConfigSaveOverrideDirectory;

        private SDVEventsModData _modConfigInformation;
        private IMonitor _monitor;
        private IModHelper _helper;
        Random randomNumberGenerator = new Random();

        int eventsRunThisSeason = 0;
        int lastRandom = 0;

        protected List<LifeEvent> activeEvents = new List<LifeEvent>();

        protected Dictionary<string, LifeEvent> eventCollection = new Dictionary<string, LifeEvent>();
        protected Dictionary<string, EventConfiguration> eventConfigurations = new Dictionary<string, EventConfiguration>();
        protected Dictionary<Season, Dictionary<string,LifeEvent>> eventsBySeason = new Dictionary<Season,Dictionary<string,LifeEvent>>();
        protected Dictionary<LifeEventType, Dictionary<string, LifeEvent>> eventsByType = new Dictionary<LifeEventType, Dictionary<string, LifeEvent>>();

        // Constructor is 'protected'
        protected EventCoordinator(SDVEventsModData configInfo, IMonitor monitor, IModHelper helper)
        {
            this._modConfigInformation = configInfo;
            this._monitor = monitor;
            this._helper = helper;

            saveDirectory = this._helper.DirectoryPath + "/" + Constants.SaveFolderName + "/";
            defaultEventConfigsDirectory = _helper.DirectoryPath + "/EventDefaults/";
            defaultEventConfigSaveOverrideDirectory = saveDirectory + "EventDefaults/";
            eventsPath =  saveDirectory + "events.json";

            this.Load();
        }

        /// <summary>
        /// If this is the first call to the Instance method, we instantiate a new EventCoordinator object and provide the passed in
        /// SDVEventsModData object.
        /// 
        /// On subsequent calls, this method will return the previously instantiated copy of EventCoordinator with the SDVEventsModData object.
        /// that was passed in the first time.
        /// </summary>
        /// <param name="configInfo">A SDVEventsModData object that defines the configuration for the mod.</param>
        /// <returns>Returns an instance of the EventCoordinator class.</returns>
        /// <exception cref="TypeInitializationException">This exception is thrown if the singleton has not already been initialized and a call to Instance() is made without passing in a valid SDVEventsModData object.</exception>
        public static EventCoordinator Instance(SDVEventsModData configInfo = null, IMonitor monitor = null, IModHelper helper = null)
        {
            //if the instance isn't null, we're already thread-safe and can simply return without
            //going through a lock.
            if (_instance == null)
            {
                lock (mutexLock)
                {
                    //now that we have a lock, check to make sure the instance is still null.
                    //this protects us from a second call to Instance() at the same time as
                    //the first that was waiting on the lock.
                    if (_instance == null)
                    {

                        if (configInfo != null && monitor != null)
                        {
                            //still null, so instantiate the eventcoordinator.
                            _instance = new EventCoordinator(configInfo, monitor,helper);
                        }
                        else
                        {
                            if (configInfo == null)
                            {
                                throw new TypeInitializationException(typeof(EventCoordinator).Name, new ArgumentNullException(nameof(configInfo), "Singleton class not yet initialized and insufficient arguments were passed to initialize a new object of this class type."));
                            }
                            else if (monitor == null)
                            {
                                throw new TypeInitializationException(typeof(EventCoordinator).Name, new ArgumentNullException(nameof(monitor), "Singleton class not yet initialized and insufficient arguments were passed to initialize a new object of this class type."));
                            }
                        }
                    }
                }               
            }

            return _instance;
        }

        private void BuildLifeEventDictionary()
        {
            
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();

            foreach(Type type in assembly.GetTypes()){
                Type baseType = typeof(LifeEvent);
                if (type.IsSubclassOf(baseType))
                {
                    string typeName = type.FullName;

                    object newObject = assembly.CreateInstance(typeName);
                    var newLifeEvent = (LifeEvent)newObject;

                    newLifeEvent.registerEventCoordinator(this);

                    eventCollection.Add(typeName, (LifeEvent)newObject);

                }
            }
        }

        private void AddEventToSeasonalDictionary(LifeEvent eventToAdd)
        {
            foreach(Season currSeason in eventToAdd.ApplicableSeasons)
            {
                eventsBySeason[currSeason].Add(eventToAdd.ClassType, eventToAdd);
            }
        }

        private void AddEventToEventTypeDictionary(LifeEvent eventToAdd)
        {
            foreach(LifeEventType currType in eventToAdd.EventTypes)
            {
                eventsByType[currType].Add(eventToAdd.ClassType, eventToAdd);
            }
        }

        private void Event_SeasonChangeEvent(object sender, EventArgsStringChanged e)
        {
            this._monitor.Log($"Reseting eventsRunThisSeason to 0");
            this.eventsRunThisSeason = 0;
        }

        public bool checkForEvent()
        {
            if (!registeredForSeasonalReset)
            {
                TimeEvents.SeasonOfYearChanged += Event_SeasonChangeEvent;
            }
            lastRandom = randomNumberGenerator.Next(minimumEventChance,maximumEventChance);

            this._monitor.Log($"lastRandom is now {lastRandom}; eventsRunThisSeason is {this.eventsRunThisSeason}");

            if(this.eventsRunThisSeason < this._modConfigInformation.maxEventsPerSeason && lastRandom <= this._modConfigInformation.eventChancePerDayAsPercentage)
            {
                this._monitor.Log($"Event should run. {this.lastRandom} <= {this._modConfigInformation.eventChancePerDayAsPercentage}");
                eventsRunThisSeason++;
                return true;
            }
            else
            {
                this._monitor.Log($"Event should not run. {this.lastRandom} > {this._modConfigInformation.eventChancePerDayAsPercentage}");
                return false;
            }
        }

        public void runEventMaintenance()
        {
            var eventsToRemove = new List<LifeEvent>();
            foreach (LifeEvent thisEvent in activeEvents)
            {
                if (thisEvent.DeregisterEventIfAppropriate())
                {
                    eventsToRemove.Add(thisEvent);
                }
            }
            foreach(LifeEvent toRemove in eventsToRemove)
            {
                activeEvents.Remove(toRemove);
            }
        }

        public bool registerEvent(LifeEvent newEvent)
        {
            try
            {
                if (newEvent.RegisterEvent(this))
                {
                    if (!activeEvents.Contains(newEvent))
                    {
                        activeEvents.Add(newEvent);
                    }
                    
                    return true;
                }
                
            } catch (Exception e)
            {
                this._monitor.Log("Error: " + e.Message);
            }
            return false;
        }

        public dynamic ReadDefaultConfiguration(String classType)
        {
            try
            {
                if (!eventConfigurations.ContainsKey(classType))
                {
                    eventConfigurations.Add(classType, SDVEventConfiguration.EventConfiguration.ReadDefaultConfiguration(defaultEventConfigsDirectory, defaultEventConfigSaveOverrideDirectory, classType));
                }

                return eventConfigurations[classType];
            }
            catch (Exception e)
            {
                throw e;
            }
            

        }

        public void SaveDefaultConfiguration(string classType, dynamic configToSave)
        {   
            try
            {
                SDVEventConfiguration.EventConfiguration.SaveDefaultConfiguration(defaultEventConfigsDirectory, defaultEventConfigSaveOverrideDirectory, classType, configToSave);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public LifeEvent getEvent(Season season, int day)
        {
            LifeEvent newEvent;
            if (RatInfestation.isRunning)
            {
                newEvent = new RobberyEvent();
            } else
            {
                newEvent = new SDVEvents.RatInfestation();
            }
             
            return newEvent;
        }

        public void Save()
        {              
            _helper.WriteJsonFile<List<LifeEvent>>(eventsPath, activeEvents);
        }

        public void Load()
        {
            BuildLifeEventDictionary();
            FileInfo eventsFile = new FileInfo(eventsPath);
            if (eventsFile.Exists)
            {
                activeEvents = JsonConvert.DeserializeObject<List<LifeEvent>>(File.ReadAllText(eventsPath), new LifeEventJSONConverter());
                if(activeEvents.Count > 0)
                {
                    foreach(LifeEvent currEvent in activeEvents)
                    {
                        this.registerEvent(currEvent);
                    }
                }
            }
        }

        public void Log(LifeEvent loggingEvent, string message,LogLevel level = LogLevel.Debug)
        {
            this._monitor.Log($"{loggingEvent.ClassType} Event: {message}", level);
        }
    }
}
