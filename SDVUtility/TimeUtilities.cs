﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardewValley;

namespace SDVUtility
{
    public class SDVDate
    {
        private const int monthsInYear = 4;
        private const int daysInMonth = 28;
        private const int daysInWeek = 7;


        private int _dayOfGame;
        /// <summary>
        /// The number of days in game this object represents.
        /// </summary>
        public int DayOfGame
        {
            get
            {
                return _dayOfGame;
            }
            set
            {
                if(value < 0)
                {
                    throw new SDVInvalidDateException("Date preceeds starting date of game.");
                } else
                {
                    _dayOfGame = value;
                }
            }
        }

        /// <summary>
        /// This StarDewValleyDate object's year.
        /// </summary>
        public int Year
        {
            get
            {
                //integer division of the current dayOfGame.  The integer portion + 1 is the current year.
                return (DayOfGame / (daysInMonth * monthsInYear)) + 1;
            }
        }

        /// <summary>
        /// This StarDewValleyDate object's month.
        /// </summary>
        public Season Season
        {
            get
            {
                //integer division current dayOfGame by the number of days in a month gives us months passed so far.
                //mod 4 the number of months to figure out the current month.                
                return SeasonHelper.getSeasonByIndex((this.DayOfGame / daysInMonth) % monthsInYear);
            }
        }

        /// <summary>
        /// This StarDewValleyDate object's Day of month.
        /// </summary>
        public int Day
        {
            get
            {
                return (DayOfGame % daysInMonth) + 1;
            }
        }

        /// <summary>
        /// Get an object represeting the first day in the game.
        /// </summary>
        public SDVDate()
        {
            DayOfGame = 0;
        }

        /// <summary>
        /// Get an object representing the specified day of game.
        /// </summary>
        /// <param name="dayOfGame">The number of days in game to represent.</param>
        public SDVDate(int dayOfGame)
        {
            this.DayOfGame = dayOfGame;
        }

        /// <summary>
        /// Get an object representing the specified date.
        /// </summary>
        /// <param name="day">Integer representing the current in game day of the month.</param>
        /// <param name="season">Season object representing the current in game season.</param>
        /// <param name="year">Integer representing the current in game year.</param>
        public SDVDate(int day, Season season, int year)
        {
            DayOfGame = (year - 1) * (daysInMonth * monthsInYear) + (season.Index() * daysInMonth) + (day - 1);
        }


        /// <summary>
        /// Get a day representing the current date.
        /// </summary>
        /// <returns>StarDewValleyDate object representing the current game date.</returns>
        static public SDVDate Now()
        {
            return new SDVDate(Game1.dayOfMonth, SeasonHelper.getSeasonByString(Game1.currentSeason), Game1.year);
        }

        /// <summary>
        /// Get a day before the current one.
        /// </summary>
        /// <returns>StarDewValleyDate represeting the day before the current day.</returns>
        public SDVDate Yesterday()
        {
            
            return new SDVDate(DayOfGame - 1);
         

        }

        /// <summary>
        /// Get a day following the current one.
        /// </summary>
        /// <returns>StarDewValleyDate represeting the day following the current day.</returns>
        public SDVDate Tomorrow()
        {
            return new SDVDate(DayOfGame + 1);
        }

        /// <summary>
        /// Get a new StarDewValleyDate Object representing the start of the current season.
        /// </summary>
        /// <returns></returns>
        public SDVDate StartOfSeason()
        {
            int newGameDay = this.DayOfGame - (this.Day - 1);
            return new SDVDate(newGameDay);
        }

        public SDVDate EndOfSeason()
        {
            int newGameDay = this.DayOfGame + (daysInMonth - (this.Day));
            return new SDVDate(newGameDay);
        }

        public SDVDate StartOfWeek()
        {
            int newGameDay = this.DayOfGame - (this.DayOfGame % daysInWeek);
            return new SDVDate(newGameDay);
        }

        public SDVDate EndOfWeek()
        {
            int newGameDay = this.DayOfGame + (6 - (this.DayOfGame % daysInWeek));
            return new SDVDate(newGameDay);
        }

        public SDVDate Add(int quantity, SDVDateInterval interval)
        {
            SDVDate dateToReturn = new SDVDate();
            switch (interval)
            {
                case SDVDateInterval.DAY:
                    dateToReturn.DayOfGame = this.DayOfGame + quantity;
                    break;
                case SDVDateInterval.WEEK:
                    dateToReturn.DayOfGame = this.DayOfGame + (quantity * daysInWeek);
                    break;
                case SDVDateInterval.SEASON:
                    dateToReturn.DayOfGame = this.DayOfGame + (quantity * daysInMonth);
                    break;
                case SDVDateInterval.YEAR:
                    dateToReturn.DayOfGame = this.DayOfGame + (quantity * (daysInMonth * monthsInYear));
                    break;
            }

            return dateToReturn;
        }

        public SDVDate Sub(int quantity, SDVDateInterval interval)
        {
            return this.Add(quantity * -1, interval);
        }

        public override string ToString()
        {
            return $"{this.Day} {this.Season.ToString()}, Year {this.Year}";
        }

        public static bool operator <(SDVDate left, SDVDate right)
        {
            if (object.ReferenceEquals(left, null) || object.ReferenceEquals(right, null))
            {
                throw new ArgumentNullException();
            }
            return left.DayOfGame < right.DayOfGame;
        }

        public static bool operator >=(SDVDate left, SDVDate right)
        {
            if (object.ReferenceEquals(left, null) || object.ReferenceEquals(right, null))
            {
                throw new ArgumentNullException();
            }
            return left.DayOfGame >= right.DayOfGame;
        }

        public static bool operator <=(SDVDate left, SDVDate right)
        {
            if (object.ReferenceEquals(left, null) || object.ReferenceEquals(right, null))
            {
                throw new ArgumentNullException();
            }
            return left.DayOfGame <= right.DayOfGame;
        }

        public static bool operator >(SDVDate left, SDVDate right)
        {
            if(object.ReferenceEquals(left,null) || object.ReferenceEquals(right, null))
            {
                throw new ArgumentNullException();
            }
            return left.DayOfGame > right.DayOfGame;
        }

        public static bool operator ==(SDVDate left, SDVDate right)
        {
            if (object.ReferenceEquals(left, null))
            {
                return object.ReferenceEquals(right, null);
            }
            return left.Equals(right);
        }

        public static bool operator !=(SDVDate left, SDVDate right)
        {
            if (object.ReferenceEquals(left, null))
            {
                return !object.ReferenceEquals(right, null);
            }
            return !left.Equals(right);            
        }

        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(null, obj))
            {
                return false;
            }
            if (System.Object.ReferenceEquals(this, obj))
            {
                return true;
            }
            try
            {
                SDVDate comparedObject = (SDVDate)obj;
                return this.DayOfGame == comparedObject.DayOfGame;                                
            }
            catch (Exception)
            {
                return false;
            }            
        }

        public override int GetHashCode()
        {
            return this.DayOfGame.GetHashCode();
        }
    }

    public enum SDVDateInterval {DAY,WEEK,SEASON,YEAR};

    public class SDVInvalidDateException : Exception
    {
        public SDVInvalidDateException() : base() { }

        public SDVInvalidDateException(string message) : base(message) { }
    }

    public static class TimeUtilities
    {
        
    }
}
