﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDVUtility
{
    public sealed class ObjectCategory
    {
        private readonly String _name;
        private readonly int _value;
        private static Dictionary<int, ObjectCategory> allCategories = new Dictionary<int, ObjectCategory>();

        public static readonly ObjectCategory GREENS = new ObjectCategory(-81, "Greens");
        public static readonly ObjectCategory GEM = new ObjectCategory(-2, "Gem");
        public static readonly ObjectCategory VEGETABLE = new ObjectCategory(-75, "Vegetable");
        public static readonly ObjectCategory FISH = new ObjectCategory(-4, "Fish");
        public static readonly ObjectCategory EGG = new ObjectCategory(-5, "Egg");
        public static readonly ObjectCategory MILK = new ObjectCategory(-6, "Milk");
        public static readonly ObjectCategory COOKING = new ObjectCategory(-7, "Cooking");
        public static readonly ObjectCategory CRAFTING = new ObjectCategory(-8, "Crafting");
        public static readonly ObjectCategory BIGCRAFTABLE = new ObjectCategory(-9, "BigCraftable");
        public static readonly ObjectCategory FRUITS = new ObjectCategory(-79, "Fruits");
        public static readonly ObjectCategory SEEDS = new ObjectCategory(-74, "Seeds");
        public static readonly ObjectCategory MINERALS = new ObjectCategory(-12, "Minerals");
        public static readonly ObjectCategory FLOWERS = new ObjectCategory(-80, "Flowers");
        public static readonly ObjectCategory MEAT = new ObjectCategory(-14, "Meat");
        public static readonly ObjectCategory FERTILIZER = new ObjectCategory(-19, "Fertilizer");
        public static readonly ObjectCategory JUNK = new ObjectCategory(-20, "Junk");
        public static readonly ObjectCategory BAIT = new ObjectCategory(-21, "Bait");
        public static readonly ObjectCategory TACKLE = new ObjectCategory(-22, "Tackle");
        public static readonly ObjectCategory SELLATFISHSHOP = new ObjectCategory(-23, "SellAtFishShop");
        public static readonly ObjectCategory FURNITURE = new ObjectCategory(-24, "Furniture");
        public static readonly ObjectCategory INGREDIENTS = new ObjectCategory(-25, "Ingredients");
        public static readonly ObjectCategory ARTISANGOODS = new ObjectCategory(-26, "ArtisanGoods");
        public static readonly ObjectCategory SYRUP = new ObjectCategory(-27, "Syrup");
        public static readonly ObjectCategory MONSTERLOOT = new ObjectCategory(-28, "MonsterLoot");
        public static readonly ObjectCategory EQUIPMENT = new ObjectCategory(-29, "Equipment");
        public static readonly ObjectCategory HAT = new ObjectCategory(-95, "Hat");
        public static readonly ObjectCategory RING = new ObjectCategory(-96, "Ring");
        public static readonly ObjectCategory WEAPON = new ObjectCategory(-98, "Weapon");
        public static readonly ObjectCategory BOOTS = new ObjectCategory(-97, "Boots");
        public static readonly ObjectCategory TOOL = new ObjectCategory(-99, "Tool");

        private ObjectCategory(int value, string name)
        {
            _value = value;
            _name = name;

            allCategories.Add(_value,this);
        }

        public static bool isFood(ObjectCategory categoryToCheck)
        {
            return (categoryToCheck.Equals(ObjectCategory.COOKING) || 
                   categoryToCheck.Equals(ObjectCategory.EGG) || 
                   categoryToCheck.Equals(ObjectCategory.FISH) || 
                   categoryToCheck.Equals(ObjectCategory.FRUITS) ||
                   categoryToCheck.Equals(ObjectCategory.GREENS) ||
                   categoryToCheck.Equals(ObjectCategory.MEAT) ||
                   categoryToCheck.Equals(ObjectCategory.VEGETABLE));
        }

        public static bool isClothing(ObjectCategory categoryToCheck)
        {
            return (categoryToCheck.Equals(ObjectCategory.BOOTS) ||
                    categoryToCheck.Equals(ObjectCategory.EQUIPMENT) ||
                    categoryToCheck.Equals(ObjectCategory.HAT) ||
                    categoryToCheck.Equals(ObjectCategory.RING));
        }

        public static ObjectCategory  findByValue(int value)
        {
            return allCategories[value];
        }

        public int GetIndex()
        {
            return _value;
        }

        public override string ToString()
        {
            return this._name;
        }

        public static bool operator ==(ObjectCategory left, ObjectCategory right)
        {
            if (object.ReferenceEquals(left, null))
            {
                return object.ReferenceEquals(right, null);
            }
            return left.Equals(right);
        }

        public static bool operator !=(ObjectCategory left, ObjectCategory right)
        {
            if (object.ReferenceEquals(left, null))
            {
                return !object.ReferenceEquals(right, null);
            }
            return !left.Equals(right);
        }

        public override bool Equals(object obj)
        {
            if (object.ReferenceEquals(null, obj))
            {
                return false;
            }
            if (System.Object.ReferenceEquals(this, obj))
            {
                return true;
            }
            try
            {
                ObjectCategory comparedObject = (ObjectCategory)obj;
                return this._value == comparedObject._value;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return this._value.GetHashCode();
        }
    }
}
