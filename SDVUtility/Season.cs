﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDVUtility
{

    public enum Season
    {
        SPRING = 0,
        SUMMER = 1,
        FALL = 2,
        WINTER = 3
    }

    public static class SeasonHelper
    {
        public static Season getSeasonByString(string seasonName)
        {   
            Season seasonToReturn;
            switch (seasonName)
            {
                case "spring":
                    seasonToReturn = Season.SPRING;
                    break;
                case "summer":
                    seasonToReturn = Season.SUMMER;
                    break;
                case "fall":
                    seasonToReturn = Season.FALL;
                    break;
                case "winter":
                    seasonToReturn = Season.WINTER;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }            
            return seasonToReturn;
        }

        public static Season getSeasonByIndex(int seasonNumber)
        {
            Season seasonToReturn;
            switch (seasonNumber)
            {
                case 0:
                    seasonToReturn = Season.SPRING;
                    break;
                case 1:
                    seasonToReturn = Season.SUMMER;
                    break;
                case 2:
                    seasonToReturn = Season.FALL;
                    break;
                case 3:
                    seasonToReturn = Season.WINTER;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            return seasonToReturn;
        }

        public static HashSet<Season> getAllSeasons()
        {
            var allSeasons = new HashSet<Season>();
            allSeasons.Add(Season.SPRING);
            allSeasons.Add(Season.SUMMER);
            allSeasons.Add(Season.FALL);
            allSeasons.Add(Season.WINTER);

            return allSeasons;
        }

        public static string ToString(this Season season)
        {
            switch (season)
            {
                case Season.SPRING:
                    return "spring";
                case Season.SUMMER:
                    return "summer";
                case Season.FALL:
                    return "fall";
                case Season.WINTER:
                    return "winter";
                default:
                    throw new ArgumentException("Unknown season provided.");
            }   
        }

        public static int Index(this Season season)
        {
            switch (season)
            {
                case Season.SPRING:
                    return 0;
                    break;
                case Season.SUMMER:
                    return 1;
                    break;
                case Season.FALL:
                    return 2;
                    break;
                case Season.WINTER:
                    return 3;
                    break;
                default:
                    throw new ArgumentException("Unknown season provided.");
            }
        }
    }
}
