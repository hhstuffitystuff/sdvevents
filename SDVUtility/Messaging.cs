﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardewModdingAPI;
using StardewModdingAPI.Events;
using StardewValley;
using Microsoft.Xna.Framework;

namespace SDVUtility
{
    public class Messaging
    {
        /// <summary>Show an message to the player with an icon to match the message type.</summary>
        /// <param name="message">The message to show.</param>
        /// <param name="duration">The number of milliseconds during which to keep the message on the screen before it fades (or <c>null</c> for the default time).</param>
        /// <remarks>Modified from ShowInfoMessage provided by Pathoschild on the Stardew Valley discord on 1/15/2017.</remarks>
        public static void ShowMessage(string message, MessageType type, int? duration = null)
        {
            var messageToSend = new HUDMessage(message, (int)type) { noIcon = false, timeLeft = duration ?? HUDMessage.defaultTime };
            ShowMessage(messageToSend);
        }

        public static void ShowMessage(HUDMessage message)
        {
            Game1.addHUDMessage(message);
        }

        /// <summary>Show an informational message to the player.</summary>
        /// <param name="message">The message to show.</param>
        /// <param name="duration">The number of milliseconds during which to keep the message on the screen before it fades (or <c>null</c> for the default time).</param>
        /// <remarks>Provided by Pathoschild on the Stardew Valley discord on 1/15/2017.</remarks>
        public static void ShowInfoMessage(string message, int? duration = null)
        {
            var messageToSend = new HUDMessage(message, (int)MessageType.Important) { noIcon = true, timeLeft = duration ?? HUDMessage.defaultTime };
            ShowMessage(messageToSend);
        }

        /// <summary>Show an error message to the player.</summary>
        /// <param name="message">The message to show.</param>
        /// <remarks>Provided by Pathoschild on the Stardew Valley discord on 1/15/2017.</remarks>
        public static void ShowErrorMessage(string message)
        {
            var messageToSend = new HUDMessage(message, (int)MessageType.Error);
            ShowMessage(messageToSend);
        }
    }

    public enum MessageType
    {
        Star = 1,
        Important = 2,
        Error = 3,
        ItemGained = 4,
        ItemLost = 5
    }
}
