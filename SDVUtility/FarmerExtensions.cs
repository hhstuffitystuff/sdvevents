﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StardewValley;
using StardewValley.Characters;

namespace SDVUtility
{
    public static class FarmerExtensions
    {

        public static bool hasCat(this Farmer farmer)
        {   
            return Game1.getFarm().characters.Concat(Utility.getHomeOfFarmer(farmer).characters).OfType<Cat>().Any();
        }

        public static bool hasDog(this Farmer farmer)
        {
            return Game1.getFarm().characters.Concat(Utility.getHomeOfFarmer(farmer).characters).OfType<Dog>().Any();
        }
    }
}
